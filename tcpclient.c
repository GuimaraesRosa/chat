#include "messageThread.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "string.h"
#include "pthread.h"
#include "errno.h"
#include "arpa/inet.h"

#define PORT 4444
#define BUF_SIZE 2000

int main(int argc, char**argv) {
	struct sockaddr_in addr, cl_addr;
	int sockfd, ret;
	char buffer[BUF_SIZE];
	char *serverAddr;
	pthread_t serverMessage;

	if (argc < 2) {
		printf("usage: client < ip address >\n");
		exit(1);
	}
	serverAddr = argv[1];
 
	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("ERROR: socket() %s\n", strerror(errno));
		exit(1);
	}
	printf("Socket created\n");

	ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
	if (ret < 0) {
		printf("ERROR: setsockopt() %s\n",strerror(errno));
		exit(1);
	}
	printf("Setsockopt done\n");

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(serverAddr);
	addr.sin_port = PORT;

	ret = connect(sockfd, (struct sockaddr *) &addr, sizeof(addr));
	if (ret < 0) {
		printf("ERROR: connect() %s\n",strerror(errno));
		exit(1);
	}
	printf("Connected to the server\n");

	memset(buffer, 0, BUF_SIZE);
	printf("Enter your messages!\n> ");

	ret = pthread_create(&serverMessage, NULL, message, (void *)(intptr_t) sockfd);
	if (ret) {
		printf("ERROR: pthread_create() %d\n", ret);
		exit(1);
	}

	while (fgets(buffer, BUF_SIZE, stdin) != NULL) {
		ret = sendto(sockfd, buffer, BUF_SIZE, 0, (struct sockaddr *) &addr, sizeof(addr));
		printf("> ");
		if (ret < 0) {
			printf("ERROR: sendto() %s\n",strerror(errno));
		}
	}

	close(sockfd);
	pthread_exit(NULL);
	return 0;
}
