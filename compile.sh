#!/bin/bash

gcc -c messageThread.c -o messageThread.o
gcc -c tcpserver.c -o tcpserver.o
gcc -c tcpclient.c -o tcpclient.o 
gcc tcpserver.o messageThread.o -o server -pthread
gcc tcpclient.o messageThread.o -o client -pthread
