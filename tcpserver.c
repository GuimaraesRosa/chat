#include "messageThread.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "sys/types.h"
#include "sys/socket.h"
#include "string.h"
#include "pthread.h"
#include "arpa/inet.h"
#include "errno.h"

#define PORT 4444
#define BUF_SIZE 2000
#define INET_ADDRLEN sizeof(struct in_addr)

void main() {
	struct sockaddr_in addr, cl_addr;
	int sockfd, len, ret, newsockfd;
	char buffer[BUF_SIZE];
	pid_t childpid;
	char clientAddr[INET_ADDRLEN];
	pthread_t clientMessage;

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd < 0) {
		printf("ERROR: socket() %s\n", strerror(errno));
		exit(1);
	}
	printf("Socket created\n");

	ret = setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));
	if (ret < 0) {
		printf("ERROR: setsockopt() %s\n",strerror(errno));
		exit(1);
	}
	printf("Setsockopt done\n");

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = PORT;

	ret = bind(sockfd, (struct sockaddr *) &addr, sizeof(addr));
	if (ret < 0) {
		printf("ERROR: bind() %s\n",strerror(errno));
		exit(1);
	}
	printf("Binding done\n");

	printf("Waiting for a connection...\n");
	ret = listen(sockfd, 5);
	if (ret < 0) {
		printf("ERROR: listen() %s\n",strerror(errno));
		exit(1);
	}

	len = sizeof(cl_addr);
	newsockfd = accept(sockfd, (struct sockaddr *) &cl_addr, &len);
	if (newsockfd < 0) {
		printf("ERROR: accept() %s\n",strerror(errno));
		exit(1);
	}

	inet_ntop(AF_INET, &(cl_addr.sin_addr), clientAddr, INET_ADDRSTRLEN);
	printf("Connection accepted from %s\n", clientAddr);

	memset(buffer, 0, BUF_SIZE);
	printf("Enter your messages!\n> ");

	ret = pthread_create(&clientMessage, NULL, message, (void *)(intptr_t) newsockfd);
	if (ret) {
		printf("ERROR: pthread_create() %d\n", ret);
		exit(1);
	}

	while (fgets(buffer, BUF_SIZE, stdin) != NULL) {
		ret = sendto(newsockfd, buffer, BUF_SIZE, 0, (struct sockaddr *) &cl_addr, len);
		printf("> ");
		if (ret < 0) {
			printf("ERROR: sendto() %s\n",strerror(errno));
			exit(1);
		}
	}

	close(newsockfd);
	close(sockfd);
	pthread_exit(NULL);
	return;
}
