#include "messageThread.h"
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"
#include "string.h"
#include "errno.h"
#include "arpa/inet.h"

#define BUF_SIZE 2000
#define RESPONSE_TEXT "\x1b[33m" //response is sent in yellow
#define RESET_TEXT "\x1b[0m"	 //reset color to white

void *message(void *socket) {

	int sockfd, ret;
	char buffer[BUF_SIZE];

	sockfd = (intptr_t) socket;
	memset(buffer, 0, BUF_SIZE);

	while (1)
	{
		ret = recv(sockfd, buffer, BUF_SIZE, 0);
		switch (ret)
		{
			case -1:
				printf("ERROR: recv() %s\n",strerror(errno));
				exit(1);
				break;
			case 0:
				printf("Lost connection\n");
				exit(1);
				break;
			default:
				printf(RESPONSE_TEXT "%s", buffer);
				setbuf(stdout, NULL);
				printf(RESET_TEXT "> ");
				break;
		}
	}
}


